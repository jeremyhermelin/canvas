'use strict';

// Declaration of the app module
var app = angular.module('app', ['main', 'ui.router']); 

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function($stateProvider, $urlRouterProvider, $locationProvider) {

	$urlRouterProvider.otherwise('/'); 

	$stateProvider
		.state({
			name: 'home',
			url: '/',  
			templateUrl: '/templates/home.html', 
			controller: 'mainCtrl'
		})
		.state({
			name: 'admin', 
			url: '/admin', 
			templateUrl: '/templates/admin.html', 
			controller: 'adminCtrl' 
		})
		.state({
			name: 'canvaquestion', 
			url: '/canvas/:canva', 
			templateUrl: 'templates/canvas.html', 
			controller: 'canvaCtrl'
		})
		.state({
			name: 'canvareport',
			params: {canva: null, report: null, templateUrl: "default.html"},  
			templateUrl: function($stateParams) {
				return 'templates/report.html';
			}, 
			controller: 'reportCtrl'
		})
		.state({
			name: 'prereport', 
			url: '/canvas/:canva/completed', 
			params: {canva: null, report: null, templateUrl: "default.html"},  
			templateUrl: function($stateParams) {
				return '/canvas-templates/' + $stateParams.templateUrl;
			}, 
			controller: 'preReportCtrl'
		})
		.state({
			name: 'modifycanva', 
			url: '/canvas/:canva/modify', 
			templateUrl: 'templates/canva-maker.html', 
			controller: 'canvaMakerCtrl'
		}); 
		
	$locationProvider.html5Mode(true); 
	
}]); 


/**
 * Declaration of modules
 */
 
var main = angular.module('main', ['ui.router', 'ngCookies']); 



// mainCtrl definition
main.controller('mainCtrl', ['$scope', '$cookies', 'api', function($scope, $cookies, api) { 
	$scope.canvas = {};  
	var reports = []; 
	if ($cookies.getObject('reports')) {
		reports = $cookies.getObject('reports'); 
	}
	api.read('canvas', function(res) {
		$scope.canvas = res.data; 
		for (var i=1; i < ($scope.canvas.length + 1); i++) {
			if (reports[i] && reports[i].canvaId == $scope.canvas[i - 1].id) {
				$scope.canvas[i - 1].finished = true; 
				$scope.canvas[i - 1].report = reports[i]; 
			}
			else $scope.canvas[i - 1].finished = false; 
		}
	});  
}]); 

// canvaCtrl definition
main.controller('canvaCtrl', ['$scope', 'api', '$stateParams', '$state', '$cookies', function($scope, api, $stateParams, $state, $cookies) {
	$scope.question = {}; 
	$scope.answer = ''; 
	var report = {"canvaId": $stateParams.canva}; 
	
	var i = 0; 
	var steps = []; 
	api.read('canvas/' + $stateParams.canva + '/steps', function(res) {
		steps = res.data; 
		if (steps[i]) $scope.question = steps[i].question; 
		else $state.go('home'); 
	}); 
	
	$scope.send = function() {
		$scope.sent = true; 
		report[steps[i].location] = $scope.answer; 
		$scope.answer = ''; 
		$scope.sent = false; 
		
		i++; 
		if(steps[i]) $scope.question = steps[i].question; 
		else {
			api.create('reports', report, function(res) {
				if (res.status == 201) {
					report = res.data; 
					var reports = $cookies.getObject('reports'); 
					if (!reports) reports = []; 
					reports[$stateParams.canva] = report; 
					$cookies.putObject('reports', reports);
					api.read('canvas/' + $stateParams.canva + '/templates', function(res) {
						$state.go('prereport', {'canva': $stateParams.canva, 'report': report, 'templateUrl': res.data[0].url}); 
					});
				}
			});  
		}
	}; 
}]); 

// reportCtrl definition
main.controller('reportCtrl', ['$scope', '$stateParams', '$state', '$cookies', 'api', function($scope, $stateParams, $state, $cookies, api) {
	$scope.report = {}; 
	$scope.validInfos = false; 
	$scope.pdfAble = false; 
	var url = '/canvas-templates/' + $stateParams.templateUrl; 
	$scope.templateUrl = url; 
	if ($stateParams.report) {
		$scope.report = $stateParams.report; 
		$scope.validInfos = true; 
	}
	else {
		var reports = $cookies.getObject('reports'); 
		if (reports[$stateParams.canva]) {
			$scope.report = reports[$stateParams.canva]; 
			$scope.validInfos = true; 
		}
		else $state.go('canvaquestion', {'canva': $stateParams.canva});  
	}
	
	
	
	$scope.makePdf = function() {
		var content = $('#pdfable').html(); 
		var pdf = { content: content}; 
		pdfMake.createPdf(pdf).open(); 
	}; 
	
	$scope.printPdf = function() {
		var content = $('#pdfable').html(); 
		var pdf = { content: content}; 
		pdfMake.createPdf(pdf).print(); 
	}; 
}]); 

// preReportCtrl definition (to test if there is a valid template in the params or get it)
main.controller('preReportCtrl', ['$stateParams', '$state', 'api', function($stateParams, $state, api) {
	if (!$stateParams.canva) {
		$state.go('home');
	}
	else {
		if ($stateParams.templateUrl == "default.html") {
			api.read('canvas/' + $stateParams.canva + '/templates', function (res) {
				$state.go('canvareport', {'canva': $stateParams.canva, 'report': $stateParams.report, 'templateUrl': res.data[0].url}); 
			});
		}
		else $state.go('canvareport', {'canva': $stateParams.canva, 'report': $stateParams.report, 'templateUrl': $stateParams.templateUrl}); 
	}
}]);

// canvaMakerCtrl definition
main.controller('canvaMakerCtrl', ['$scope', 'api', '$stateParams', '$state', function($scope, api, $stateParams, $state) {
	$scope.canva = {};   
	$scope.steps = []; 
	$scope.template = {}; 
	if ($stateParams.canva) {
		api.read('canvas/' + $stateParams.canva, function(res) {
			$scope.canva = res.data; 
		}); 
		api.read('canvas/' + $stateParams.canva + '/steps', function(res) {
			$scope.steps = res.data; 
		}); 
		api.read('canvas/' + $stateParams.canva + '/templates', function(res) {
			if (!res.data[0]) $scope.template = {url: "", canvaId: $stateParams.canva}; 
			else $scope.template = res.data[0]; 
		}); 
	}

	$scope.send = function() {
		$scope.sent = true; 
		if ($stateParams.canva) {
			api.update('canvas/' + $scope.canva.id, $scope.canva, function(res) {});
			if ($scope.template.id) api.update('templates/' + $scope.template.id, $scope.template, function(res) {}); 
			else {
				api.create('templates', $scope.template, function(res) {
					$scope.template = res.data[0]; 
				}); 
			}
			for (var i=0; i < $scope.steps.length; i++) {
				api.update('steps/' + $scope.steps[i].id, $scope.steps[i], function(res) {}); 
			}
			$state.go('admin');
		}
	};
	
	$scope.removeCanva = function() {
		api.delete('canvas/' + $scope.canva.id, function(res) {
			if (res.status == 200 || res.status == 304) {
				$state.go('admin'); 
			}
		}); 
	};  
	
	$scope.addStep = function() { 
		
		var newStep = {canvaId: $stateParams.canva, question: {main: '', small: ''}, location: ''}; 
    	
		api.create('steps', newStep, function(res) {
			if(res.status == 201) {
				$scope.steps.push(res.data); 
			}
		});  
	}; 
	
	$scope.removeStep = function(step) {
		api.delete('steps/' + step.id, function(res) {
			if (res.status == 200 || res.status == 304) {
				$scope.steps.splice($scope.steps.indexOf(step), 1); 
			}
			else {
				$scope.steps.splice($scope.steps.indexOf(step), 1); 
				console.log('IMPORTANT voilà le statut de la requête lors d\'un api.delete : ' + res.status); 
			}
		}); 
	}; 
}]); 

// adminCtrl definition
main.controller('adminCtrl', ['$scope', '$state', 'api', function($scope, $state, api) { 
	$scope.canvas = {}; 
	api.read('canvas', function(res) {
		$scope.canvas = res.data; 
	}); 
	
	$scope.addCanva = function() {
		var newCanva = {
    		title: 'Nouveau canva', 
			description:"Un nouveau canva"
		};  
    	
		api.create('canvas', newCanva, function(res) {
			if(res.status == 201) {
				$scope.canvas[res.data.id] = res.data; 
				$state.go('modifycanva', {'canva': res.data.id}); 
			}
		});  
	}; 
}]); 

// api service definition
main.provider('api', function() {
	
	var apiDomain = 'http://localhost:3000/'; 
	
	this.setApiDomain = function(_apiDomain) {
		apiDomain = _apiDomain; 
	}; 
	
	var error = function(response) {
		if (response.status) {
			console.log('error ' + response.status); 
		}
		else console.log('unidentified error'); 
	}; 
	
	function Factory($http) {
		this.patch = function(ressource, data, callback) {
			$http.patch(apiDomain + ressource, data).then(callback, error); 
		}; 
		this.create = function(ressource, data, callback) {
			$http.post(apiDomain + ressource, data).then(callback, error); 
		}; 
		this.read = function(ressource, callback) {
			$http.get(apiDomain + ressource).then(callback, error); 
		}; 
		this.update = function(ressource, data, callback) {
			$http.put(apiDomain + ressource, data).then(callback, error); 
		}; 
		this.delete = function(ressource, callback) {
			$http.delete(apiDomain + ressource).then(callback, error); 
		}; 
	}
	
	this.$get = function($http) {
		return new Factory($http); 
	}; 
}); 

// <form-element> directive
main.directive('formElement', function() {
	return {
		restrict: 'E', 
		templateUrl: 'templates/form.html', 
	}; 
}); 