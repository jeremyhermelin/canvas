// built-in includes
var path = require('path');
var http = require('http'); 

// dependancies includes
var express = require('express');
var bodyParser = require('body-parser'); 

// database emulation (for dev purpose)
var DB = {
	canvas : {
    	'1': {
    		id: '1', 
    		title: 'Les 3 questions en or', 
    		description:"D'après le cours Business3g de Laurent Chenot", 
    		template: "<p>Je suis {{content.me}} et j'aide {{content.clients}} à {{content.problem}}</p>", 
    		templateUrl: 'test.html', 
    		steps: [
    			{
    				question: {main: 'Quel est le problème que vous résolvez ? ', small: 'Ex: Casser des briques'},	
    				location: 'problem'
    			}, 
				{
					question: {main: 'Qui êtes vous ? ', small: "L'expert immobilier"}, 
					location: 'me'
				}, 
				{
					question: {main: 'Qui aidez vous ? ', small: 'Les personnes handicapées'}, 
					location: 'clients'
				}
			]
		}, 
    	'2': {
    		id: '2', 
    		title: 'Structure MERE', 
    		description:"D'après le cours de David Jay, la révolution vidéo", 
    		template: "<ul><li>MOTIVATION : {{content.M}}</li><li>EXPLICATION : {{content.E1}}</li><li>RECETTE : {{content.R}}</li><li>EXERCICE : {{content.E2}}</li></ul>", 
    		templateUrl: 'mere.html', 
    		steps: [
    			{
    				question: {main: 'Motivation : ', small: ''}, 
    				location: 'M'	
    			}, 
    			{
    				question: {main: 'Explication : ', small: ''}, 
    				location: 'E1'
    			}, 
    			{
    				question: {main: 'Recette : ', small: ''}, 
    				location: 'R'
    			}, 
    			{
    				question: {main: "Exercice : ", small: ''}, 
    				location: 'E2'
    			}
    		]
    	}
    }, 
    users : {}
}; 


var app = express();
var api = express.Router(); 

app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({extended: true})); 
/*
 *    MAIN Routing
 */


app.use('/api', api); 

// serving static files (including static client application)
app.use(express.static(__dirname + '/static/app'));

app.get('*', function (req, res) {
	res.sendFile('static/app/app.html', { root: __dirname }); 
	console.log('Client app file sent'); 
}); 


/*
 *    API Routing
 */


api.route('/canvas')

	.get(function (req, res) {
		res.json(DB.canvas); 
		console.log('API call : GET canvas'); 
	})
	
	.post(function (req, res) {
		var id = newId(); 
		var canva = req.body; 
		canva.id = id; 
		DB.canvas[id] = canva; 
		res.json({saved: true, id: id}); 
		console.log('API call : POST canvas'); 
		console.log('new canva on : canvas/' + id); 
	}); 

api.route('/canvas/:canva')

	.get(function(req, res) { 
    	res.json(DB.canvas[req.params.canva]);
		console.log('API call : GET canvas/' +  req.params.canva); 
	})
	
	.put(function(req, res) {
		DB.canvas[req.params.canva] = req.body; 
		res.json({saved: true}); 
		console.log('API call : PUT canvas/' + req.params.canva); 
	}); 

api.route('/canvas/:canva/template')

	.get(function(req, res) {
		res.json(DB.canvas[req.params.canva].templateUrl);
		console.log('API call : GET canvas/' + req.params.canva + '/template');  
	}); 




/*
 * Server startup
 */

var server = http.Server(app); 

server.listen(8080, function() {
    console.log('App listening on port %s', 8080);
});


var newId = function() {
	var id = 1; 
	while (DB.canvas[id]) {
		id++; 
	}
	return id; 
}; 